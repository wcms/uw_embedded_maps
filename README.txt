This module provides maps (currently Google maps) widget support within CKeditor in the WYSIWYG module, using the platform's provided widgets, without having to allow <script> tags.

Once you enable this module, you should add the Google Maps buttons from the WYSIWYG profiles for CKEditor for your text formats, and enable the CKEditor link filter within your text formats. If you are filtering allowed HTML tags and/or attributes, you must allow the following tags and attributes to bypass the filter.

* ckgoogleMaps[data-src|data-height]


This module will replace the custom tags with JavaScript widgets from the map site. If JavaScript is disabled, the widgets will fall back to a link to the content.

Google maps widget dialog box options:

GOOGLE MAPS

1. Google maps url:
   This is your page URL, with the link starting with www.google.com/maps

2. Height:
   The height of the embed on the page
