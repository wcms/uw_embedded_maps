/**
 * @file
 * Define tags as "block level" so the editor doesn't try to put paragraph tags around them.
 */

var cktags = ['ckembeddedmaps'];
// Global variable used to store fakeImage element when maps is double clicked.
var doubleclick_element;
CKEDITOR.plugins.add('embeddedMaps', {
    requires: ['dialog', 'fakeobjects'],
    init: function (editor) {
        // Set up object to share variables amongst scripts.
        CKEDITOR.embeddedMaps = {};
        // Register button for embedded maps.
        editor.ui.addButton('Embedded Maps', {
            label: "Add/Edit Embedded Maps",
            command: 'embeddedMaps',
            icon: this.path + 'icons/embedded_maps.png'
        });
        // Register right-click menu item for Google maps.
        editor.addMenuItems({
            embeddedMaps: {
                label: "Edit Embedded Maps",
                icon: this.path + 'icons/embedded_maps.png',
                command: 'embeddedMaps',
                group: 'image',
                order: 1
            }
        });
        // Make ckembeddedmap to be a self-closing tag.
        CKEDITOR.dtd.$empty['ckembeddedmaps'] = 1;
        // Make sure the fake element for Google Maps has a name.
        CKEDITOR.embeddedMaps.ckembeddedmaps = 'Embedded maps widget';
        CKEDITOR.lang.en.fakeobjects.ckembeddedmaps = CKEDITOR.embeddedMaps.ckembeddedmaps;
        // Add JavaScript file that defines the dialog box for Google maps.
        CKEDITOR.dialog.add('embeddedMaps', this.path + 'dialogs/embeddedMaps.js');
        // Register command to open dialog box when button is clicked.
        editor.addCommand('embeddedMaps', new CKEDITOR.dialogCommand('embeddedMaps'));

        // Regular expressions for Google maps and Google My Maps url.
        CKEDITOR.embeddedMaps.googleMaps_url_regex = /^(?:https?:\/\/)?(?:www\.)?google\.(?:ca|com)\/maps\/embed\?pb=([^"]*)$/;
        CKEDITOR.embeddedMaps.googleMyMaps_url_regex = /^(?:https?:\/\/)?(?:www\.)?google\.(?:ca|com)\/maps\/d\/u\/\d\/embed\?mid=([^"]*)$/;

        // Open the appropriate dialog box if an element is double-clicked.
        editor.on('doubleclick', function (evt) {
            var element = evt.data.element;

            // Store the element as global variable to be used in onShow of dialog, when double clicked.
            doubleclick_element = element;

            if (element.is('img') && element.data('cke-real-element-type') === 'ckembeddedmaps') {
                evt.data.dialog = 'embeddedMaps';
            }
        });

        // Add the appropriate right-click menu item if an element is right-clicked.
        if (editor.contextMenu) {
            editor.contextMenu.addListener(function (element, selection) {
                if (element && element.is('img') && element.data('cke-real-element-type') === 'ckembeddedmaps') {
                    return {embeddedMaps: CKEDITOR.TRISTATE_OFF};
                }
            });
        }

        // Add CSS to use in-editor to style custom (fake) elements.
        CKEDITOR.addCss(
            'img.ckembeddedmaps {' +
            'background-image: url(' + CKEDITOR.getUrl(this.path + 'icons/big/embedded_maps.png') + ');' +
            'background-position: center center;' +
            'background-repeat: no-repeat;' +
            'background-color: #000;' +
            'width: 100%;' +
            'height: 350px;' +
            'margin: 0 0 10px 0;' +
            'margin-left: auto;' +
            'margin-right: auto;' +
            '}' +

            // Google Maps: Definitions for wide width.
            '.uw_tf_standard_wide p img.ckembeddedmaps {' +
                'height: 350px;' +
            '}' +

            // Google Maps: Definitions for standard width.
            '.uw_tf_standard p img.ckembeddedmaps {' +
                'height: 350px;' +
            '}'
        );
    },
    afterInit: function (editor) {
        // Make fake image display on first load/return from source view.
        if (editor.dataProcessor.dataFilter) {
            editor.dataProcessor.dataFilter.addRules({
                elements: {
                    ckembeddedmaps: function (element) {
                        // Reset title.
                        CKEDITOR.lang.en.fakeobjects.ckembeddedmaps = CKEDITOR.embeddedMaps.ckembeddedmaps;
                        // Adjust title if a list name is present.
                        if (element.attributes['data-src']) {
                            CKEDITOR.lang.en.fakeobjects.ckembeddedmaps += ': ' + element.attributes['data-src'];
                        }
                        // Note that this just accepts whatever attributes are on the element; may want to filter these.
                        var embeddedMaps_return = editor.createFakeParserElement(element, 'ckembeddedmaps', 'ckembeddedmaps', false);
                        // Set the fake object to the entered height; if there isn't one, use 150 so it's not invisible.
                        if (!element.attributes['data-height']) {
                            element.attributes['data-height'] = '150';
                        }
                        embeddedMaps_return.attributes.height = element.attributes['data-height'];
                        return embeddedMaps_return;
                    }
                }
            });
        }
    }
});
