/**
 * @file
 */

(function () {
  // Variable used to track when the form is submitted.
  // i.e to check for double submissions on using enter key in dialog.
  var formSubmitted = false;
  var embeddedMapsDialog = function (editor) {
    return {
      title: 'Embedded Maps Properties',
      minWidth: 625,
      minHeight: 150,
      contents: [{
        id: 'embeddedMaps',
        label: 'Embedded Maps',
        elements: [{
          type: 'select',
          id: 'embeddedMapsTypeInput',
          label: 'Embedded Map Type',
          items: [
            ['Google Maps', 'googleMaps'],
            ['Google My Maps', 'googleMyMaps'],
          ],
          required: true,
          setup: function (element) {
            this.setValue(element.getAttribute('data-type'));
          },
          onChange: function () {
            adjustForType();
          }
        }, {
          type: 'html',
          id: 'description',
          html: '',
        }, {
          type: 'text',
          id: 'embeddedMapsURLInput',
          label: 'Google Maps Embedded URL: (starts with https://www.google.com/maps/embed?pb=)',
          required: true,
          setup: function (element) {
            this.setValue(element.getAttribute('data-src'));
          }
        }, {
          type: 'text',
          id: 'heightInput',
          label: 'Maps height in pixels (minimum 150): (required)',
          required: true,
          setup: function (element) {
            this.setValue(element.getAttribute('data-height'));
          }
        }]
      }],
      onOk: function () {
        // Get form information.
        var embeddedMapsURLInput = this.getValueOf('embeddedMaps', 'embeddedMapsURLInput');
        var embeddedMapsTypeInput = this.getValueOf('embeddedMaps', 'embeddedMapsTypeInput');
        var heightInput = this.getValueOf('embeddedMaps', 'heightInput');
        // Validate input. Note that there is probably a CKEditor specific way to do this, but this works.
        errors = '';

        if (embeddedMapsTypeInput == 'googleMaps' && !CKEDITOR.embeddedMaps.googleMaps_url_regex.test(embeddedMapsURLInput)) {
          errors += "You must enter a valid Google Maps URL.\r\n";
        }
        if (embeddedMapsTypeInput == 'googleMaps' && !embeddedMapsURLInput) {
          errors += "You must enter the Google Maps URL.\r\n";
        }
        if (embeddedMapsTypeInput == 'googleMyMaps' && !CKEDITOR.embeddedMaps.googleMyMaps_url_regex.test(embeddedMapsURLInput)) {
          errors += "You must enter a valid Google My Maps URL.\r\n";
        }
        if (embeddedMapsTypeInput == 'googleMyMaps' && !embeddedMapsURLInput) {
          errors += "You must enter the Google My Maps URL.\r\n";
        }
        if (!heightInput || heightInput.NaN || heightInput < 100 || Math.floor(heightInput) != heightInput) {
          errors += "You must enter a valid whole number for the height.\r\n";
        }
        // If form has been submitted before then set it back to not being seeing before.
        // i.e if this is double submission set it back to not being run before.
        if (formSubmitted == true) {
          formSubmitted = false;
          return false;
        }
        // Only display erros if there are errors to display and the form has not been run before.
        else if (errors && formSubmitted == false) {
          alert(errors);
          formSubmitted = true;
          return false;
        }
        else {
          // Create the Google map element.
          var ckembeddedMapsNode = new CKEDITOR.dom.element('ckembeddedmaps');
          // Save contents of dialog as attributes of the element.
          ckembeddedMapsNode.setAttribute('data-type', embeddedMapsTypeInput);
          ckembeddedMapsNode.setAttribute('data-src', embeddedMapsURLInput);
          ckembeddedMapsNode.setAttribute('data-height', heightInput);
          // Adjust title based on user input.
          CKEDITOR.lang.en.fakeobjects.ckembeddedmaps = CKEDITOR.embeddedMaps.ckembeddedmaps + ': ' + embeddedMapsTypeInput;
          // Create the fake image for this element and insert into the document (realElement, className, realElementType, isResizable).
          var newFakeImage = editor.createFakeElement(ckembeddedMapsNode, 'ckembeddedmaps', 'ckembeddedmaps', false);
          // Set the fake object to the entered height; if there isn't one, use 100 so it's not invisible.
          newFakeImage.addClass('ckembeddedmaps');
          if (this.fakeImage) {
            newFakeImage.replace(this.fakeImage);
            editor.getSelection().selectElement(newFakeImage);
          }
          else {
            editor.insertElement(newFakeImage);
          }
          // Reset title.
          CKEDITOR.lang.en.fakeobjects.ckembeddedmaps = CKEDITOR.embeddedMaps.ckembeddedmaps;
        }
      },
      onShow: function () {
        // Set up to handle existing items.
        this.fakeImage = this.ckembeddedMapsNode = null;
        var fakeImage;

        // Check if element is right clicked or icon was clicked, if not use global varaible doubleclick_element.
        if (this.getSelectedElement()) {
          fakeImage = this.getSelectedElement();
        }
        else {
          fakeImage = doubleclick_element;
        }

        if (fakeImage && fakeImage.data('cke-real-element-type') && fakeImage.data('cke-real-element-type') === 'ckembeddedmaps') {
          this.fakeImage = fakeImage;
          var ckembeddedMapsNode = editor.restoreRealElement(fakeImage);
          this.ckembeddedMapsNode = ckembeddedMapsNode;
          this.setupContent(ckembeddedMapsNode);
        }
      }
    }
  }
  CKEDITOR.dialog.add('embeddedMaps', function (editor) {
    return embeddedMapsDialog(editor)
  });
  function adjustForType() {
    // Show and hide form elements based on which Twitter type is selected.
    var dialog = CKEDITOR.dialog.getCurrent();
    var embeddedMapsTypeInput = dialog.getValueOf('embeddedMaps','embeddedMapsTypeInput');
    var embeddedMapsURLInput = dialog.getContentElement('embeddedMaps','embeddedMapsURLInput').getElement();
    // googleMyMapsInput = dialog.getContentElement('embeddedMaps','googleMyMapsInput').getElement();
    var heightInput = dialog.getContentElement('embeddedMaps','heightInput').getElement();
    var description = dialog.getContentElement('embeddedMaps','description').getElement();
    if (embeddedMapsTypeInput == 'googleMaps') {
      embeddedMapsURLInput.show();
      heightInput.show();
      description.setHtml('Copy the embedded code from the Google maps.');
      // Change the label.
      embeddedMapsURLInput.getFirst().setText('Google Maps Embedded URL: (starts with https://www.google.com/maps/embed?pb=');
    }
    else if (embeddedMapsTypeInput == 'googleMyMaps') {
      embeddedMapsURLInput.show();
      heightInput.show();
      description.setHtml('Copy the embedded code from the Google My Maps.');
      // Change the label.
      embeddedMapsURLInput.getFirst().setText('Google My Maps Embedded URL: (starts with https://www.google.com/maps/d/u/1/embed?mid=');

    }
  }
})();
